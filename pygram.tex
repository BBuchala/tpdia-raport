\part{Pygrametl}

\section{Wstęp}

\textbf{Pygrametl} jest strukturą (ang. \textit{framework}) dla języka programowania Python dostarczającą funkcje powszechnie używane podczas procesów ekstrakcji danych (ang. \textit{Extract-Transform-Load}, w skrócie ETL). Jest to pakiet otwartoźródłowy, rozprzestrzeniany na licencji BSD. Pygrametl został stworzony oraz jest utrzymywany przez członków Uniwersytetu w Aalborgu: Christiana Thomsena, Johannesa Lindhart Borresena, Ove Andersena oraz Sørena Kejser Jensena. Głównym powodem stworzenia pakietu było dotychczasowe podejście producentów dostarczających narzędzia do pracy z procesami ekstrakcji danych. \\

Procesy ETL używane są w celu ekstrakcji danych, transformacji oraz ładowania ich do hurtowni danych (ang. \textit{data warehouses}). Większość rozwiązań, zarówno komercyjnych jak i tych otwartoźródłowych (w tym omawiany wcześniej Oracle) dostarcza w tym celu graficzny interfejs użytkownika (ang. \textit{Graphical User Interface}, w skrócie GUI), w którym programista może wizualnie zdefiniować przepływ danych i wykonywane operacje. Zaletą korzystania z GUI jest konieczność posiadania jedynie niewielkiej wiedzy z zakresu programowania oraz duża przejrzystość zaprojektowanego procesu ETL. Graficzny interfejs niesie jednak ze sobą wady, najważniejszymi z nich są:
\begin{itemize}
	\item Trudności w zaprojektowaniu rozwiązania dla skomplikowanych problemów przy użyciu ograniczonych komponentów dostarczanych przez graficzny edytor.
	\item Duża złożoność czasowa podczas implementacji skomplikowanych problemów.
	\item Brak dostępu do kodu źródłowego, co uniemożliwia dokładne dopasowanie istniejących komponentów do natury problemu.
\end{itemize}

Założeniem twórców struktury było zwiększenie produktywności poprzez wykonywanie operacji na danych na niższym poziomie abstrakcji. Pygrametl oferuje oryginalne podejście, dostarczając programiście określone funkcje w języku Python, chowając jednocześnie dostęp do leżących u podstaw tabel hurtowni danych. W szczególności ułatwia to pracę z wymiarami danych ułożonymi w schemacie płatka śniegu, gdyż deweloper operuje na jednym ,,obiekcie wymiarowym'' dla całego płatka śniegu, podczas gdy Pygrametl zajmuje się pozostałymi tabelami hurtowni w tym płatku. Dodatkowym uproszczeniem jest wstawianie danych do wymiaru oraz tabeli faktów (opisanych później) poprzez iterację danych źródłowych. Pygrametl przystosowany jest do pracy z CPythonem oraz Jythonem, co pozwala na użycie kodu Javy i sterowników JDBC do pracy nad programem ETL.

\section{Instalacja i uruchamianie}

Ponieważ Pygrametl jest pakietem języka Python, przed użyciem dyrektywy \texttt{import} należy dokonać instalacji pakietu. Pygranetl wymaga zainstalowanej implementacji języka Python w przynajmniej jednej z 3 form:

\begin{itemize}
	\item Jython, wersja 5.3.2 lub nowsza
	\item Python 2, wersja 2.6 lub nowsza
	\item Python 3, wersja dowolna
\end{itemize}

Instalacji pakietu z PyPI (\textit{Python Package Index}, oficjalne repozytorium bibliotek dla języka Python) można dokonać przy użyciu menedżera pakietów takiego jak Conda czy Pip lub poprzez ręczne pobranie najnowszej wersji pakietu z serwisu Github. \\

Poprawność instalacji Pygrametl można zweryfikować poprzez uruchomienie interpretera języka Python w konsoli lub terminalu i wpisanie komendy \texttt{import pygrametl}. Jeżeli pakiet został zainstalowany niepoprawnie, zwrócona zostanie informacja o błędzie.

\subsection{Instalacja za pomocą Pip}

Instalacja Pygrametl z PyPI może zostać wykonana globalnie (dostępna dla każdego) lub lokalnie w katalogu domowym użytkownika. Globalna instalacja na większości systemów będzie wymagała uprawnień administratora lub roota, instalacja lokalna nie posiada takiego wymogu.

\begin{lstlisting}[language=Python]
# Instalacja globalna
pip install pygrametl

# Instalacja lokalna
pip install pygrametl --user
\end{lstlisting}

\subsection{Instalacja za pomocą Condy}

Conda jest alternatywnym menedżerem pakietów dla Pythona, dostarczanym w dystrybucji Pythona Anaconda (stworzonym przez \textit{Continuum Analytics}). Na chwilę obecną Pygrametl nie dostarcza pakietu dla tego menedżera (format pakietów jest nieco inny niż dla Pip). Istnieje jednak możliwość własnoręcznego zbudowania, a następnie instalacji pakietu. 

\begin{lstlisting}[language=Python]
# Stworzenie szablonu dla pakietu Condy z pakietu PyPI
$ conda skeleton pypi pygrametl

# Zbudowanie pakietu Condy
$ conda build

# Instalacja pakietu
$ conda install --use-local pygrametl
\end{lstlisting}

\subsection{Instalacja z serwisu Github}

Ostatnia możliwość zakłada pobranie kodu źródłowego z oficjalnego repozytorium na serwisie Github. Ponieważ projekt korzysta z systemu kontroli wersji Git, repozytorium może zostać sklonowane za pomocą następującej komendy:

\begin{lstlisting}[language=bash]
$ git clone https://github.com/chrthomsen/pygrametl.git
\end{lstlisting} 

\section{Omówienie funkcjonalności}

\subsection{Zarys}\label{sec:pyZarys}

Podczas używania Pygrametl, programista jest odpowiedzialny za utworzenie kodu kontrolującego przepływ danych:
\begin{itemize}
	\item Ekstrakcja danych z systemów źródłowych (mogą to być przykładowo bazy danych lub pliki).
	\item Transformacja (przekształcenie) danych źródłowych.
	\item Załadowanie przetworzonych danych do hurtowni.
\end{itemize}

Zwłaszcza pierwsza i ostatnia operacja zostały w znacznym stopniu uproszczone. Zasadniczą ideą jest stworzenie przez dewelopera obiektów reprezentujących każdą tabelę faktów oraz wymiar istniejący w hurtowni danych. Obiekt symbolizujący wymiar udostępnia wygodne w użyciu metody jak \texttt{insert, lookup} i im podobne, chowając przy tym detale związane z niskopoziomową implementacją (w tym szczegóły powiązane z użyciem pamięci podręcznej i tworzenia kluczy). Warto zauważyć, że wymiar zbudowany na schemacie płatka śniegu traktowany jest w podobny sposób -- pojedynczy obiekt reprezentujący cały wymiar, pomimo faktu podziału danych na wiele leżących u podstaw tabel. \\

Obiekt reprezentujący wymiar przyjmuje wiersze (czyli pojedyncze rekordy, ang. \textit{row}) jako argumenty. Wiersz w Pygrametl jest prostym mapowaniem pomiędzy nazwą, a wartością. Autorzy tego narzędzia postanowili skorzystać z dynamicznego typowania języka Python -- Pygrametl nie dokonuje sprawdzenia, czy wszystkie wczytywane do wymiaru wiersze dysponują tym samym zestawem atrybutów oraz czy atrybuty te posiadają zgodne typy. W przypadku gdy taka procedura jest wymagana w trakcie tworzenia kodu, programista powinien sam zadbać o kontrolę. Istnieje wtedy możliwość przypisania wartości domyślnych dla części atrybutów lub pominięcie fragmentów wczytywanych wierszy. Problemy pojawiają się jedynie w przypadku, gdy system próbuje wczytać brakujące wartości. Dodatkowym ograniczeniem jest zablokowanie możliwości wstawiana do hurtowni danych, które nie istnieją (Pygrametl nie dokonuje predykcji na podstawie posiadanych informacji) -- w takim przypadku trzeba zadbać o wyżej wymienione wartości domyślne. \\ 

Pygrametl został zaimplementowany jako moduł języka programowania Python -- wyboru dokonano ze względu na dużą popularność języka oraz mnogość dodatkowych bibliotek, które mogą wspierać proces rozwój oprogramowania. Python jest językiem dynamicznie typowanym (programista nie musi deklarować typu zmiennych) oraz silnie typowanym (jeżeli zmienna przechowuje wartość liczbową, nie można jej traktować jako napis). Pygrametl oprócz tego korzysta z możliwości deklarowania domyślnych wartości oraz wsparcia dla programowania funkcyjnego (co m. in. pozwala na użycie funkcji lub wyrażeń lambda jako argumentów).

\subsection{Wspierane źródła danych}

Jak wspomniano w podrozdziale \ref{sec:pyZarys}, w Pygrametl dane przetrzymywane i przetwarzane są jako wiersze. Zamiast implementować własne klasy wierszy, można skorzystać z wbudowanych w język Python klas słownikowych, które dostarczają wydajne mapowania pomiędzy kluczami (zwane również nazwami atrybutów) a wartościami (wartości atrybutów). Dane przekazywane są ze źródeł bezpośrednio do słowników -- jest to powód, dla którego źródło musi bć iterowalne (jego klasa musi posiadać zdefiniowaną metodę \texttt{\_iter\_}). Taka restrykcja sprawia, że możliwe jest zastosowanie pętli \texttt{for} na źródle, co ułatwia odczyt wierszy.

Pygrametl posiada dwie główne klasy służące do wczytywania danych oraz kilka pomniejszych będących ich modyfikacjami.

\begin{description}
	\item[\texttt{SQLSource}] Źródło danych zwracające wiersze z zapytania w języku SQL. Samo zapytanie, połączenie z bazą danych oraz opcjonalnie nowe nazwy kolumn są podawane w momencie wywoływania inicjalizacji źródła.
	\item[\texttt{CSVSource}] Źródło danych zwracające pojedynczy wiersz na każdą linię znajdującą się w pliku .csv, którego dane rozdzielone są ogranicznikiem (znakiem rozgraniczającym -- sam znak można wybrać przy inicjalizacji). Klasa ta jest tak naprawdę referencją do klasy csv.DictReader w standardowej bibliotece Pythona.
	\item Reszta klas pozwalających na wczytanie danych (takie jak \texttt{MergeJoiningSource} czy \texttt{HashJoiningSource}) są modyfikacjami klasy \texttt{CSVSource} pozwalającymi miedzy innymi łączyć dane z wielu plików czy dokonywać wstępnej filtracji lub transformacji danych.
\end{description}

\subsection{Wymiary}

\subsubsection{Hierarchia}

W Pygrametl wymiary (tabele) bazy danych reprezentowane są przez obiekty klasy \texttt{Dimension} i jej pochodnych. Rysunek \ref{img:pygram01} reprezentuje hierarchię dziedziczności dla klas wymiarów (oprócz tego po klasie bazowej dziedziczy również klasa \texttt{BulkDimension}). Pokazane są jedynie metody publiczne, oraz ich argumenty które muszą być podane przy ich wywołaniu (nie posiadające wartości domyślnych). Należy zauważyć, że klasa \texttt{SnowflakedDimension} nie dziedziczy bezpośrednio po klasie \texttt{Dimension}, a jedynie udostępnia identyczny interfejs. Dynamiczne typowanie języka Python zezwala na odpytywanie jej, jakby była klasą pochodną klasy \texttt{Dimension}. 

\begin{figure}
	\centering
	\includegraphics[width=0.8\textwidth]{./img/pygram01.png}
	\caption{Hierarchia dziedziczności dla klas wspierających pracę z wymiarami.}
	\label{img:pygram01}
\end{figure}

\subsubsection{Klasa Dimension}

\texttt{\textbf{Dimension}} jest najbardziej podstawową klasą służącą do reprezentacji wymiaru hurtowni danych w Pygrametl. Wykorzystywana jest do jednowymiarowych hurtowni (zawierających dokładnie jedną tabelę). W momencie tworzenia instancji klasy należy podać:
\begin{itemize}
	\item Nazwę reprezentowanego wymiaru.
	\item Nazwę kolumny zawierającej klucz główny.
	\item Listę atrybutów.
\end{itemize}

Dodatkowe elementy, których zdefiniowanie nie jest obligatoryjne:
\begin{itemize}
	\item Atrybuty podglądu -- podzbiór kolumn, które mogą służyć do podglądu kluczy.
	\item Metoda \texttt{idfinder} -- jest to metoda wyliczająca wartość klucza głównego dla wiersza, który go nie posiada. Wywoływana w momencie próby wstawienia danych bez klucza do hurtowni.
	\item Wartość domyślna klucza -- w przypadku nieudanego podglądu.
	\item Metoda \texttt{rowexpander} -- funkcja rozszerzająca wiersz o dodatkowe atrybuty (jeżeli istnieje taka potrzeba).
\end{itemize}

Przykładowo definicja obiektu klasy \texttt{Dimension} może wyglądać tak:

\lstinputlisting[language=Python]{./listings/example01.py}

Podstawowe metody oferowane przez klasę \texttt{Dimension}:
\begin{itemize}
	\item \texttt{lookup} -- wykonuje projekcję na wierszu podanym jako argument (wiersz taki musi posiadać kolumny oznaczone jako atrybuty podglądu). Zwraca klucz główny wiersza.
	\item \texttt{getbykey} -- przeciwieństwo funkcji \texttt{lookup}. Przyjmuje jako argument klucz, zwraca wiersz ze wszystkimi atrybutami.
	\item \texttt{getbyvalue} -- zwraca wszystkie wiersze, których podzbiory kolumn posiadają identyczne wartości jak te podane jako atrybuty.
	\item \texttt{insert} -- dodawanie nowych elementów do wymiaru (tabeli w hurtowni). Jako atrybut przyjmuje wiersz -- w przypadku gdy nie posiada on klucza, wywoływana jest metoda \texttt{idfinder}.
	\item \texttt{update} -- aktualizuje wartości wybranych kolumn dla wierszy o kluczu podanym jako parametr.
	\item \texttt{ensure} -- połączenie metod \texttt{lookup} oraz \texttt{insert}. Funkcja ta w pierwszej kolejności dokonuje podglądu w celu znalezienia klucza szukanego rekordu. Jeżeli szukany wiersz nie istnieje (przy jednoczesnym braku zdefiniowanej wartości domyślnej), metoda wstawia brakujący wiersz. Zwracanym wynikiem zawsze jest klucz główny. W przypadku, gdy zadeklarowano metodę \texttt{rowexpaner}, funkcja ta jest wywoływana przez \texttt{ensure} w celu uzupełnienia brakujących pól przed dołożeniem nowego rekordu do hurtowni.
\end{itemize}

\subsubsection{Klasy pochodne}

Większość klas pochodnych została zaimplementowana w celu obsłużenia konkretnych przypadków.

\begin{description}
	\item[\texttt{CachedDimension}] udostępnia identyczne metody jak klasa \texttt{Dimension}. Zasadniczą różnicą jest użycie pamięci podręcznej w celu przyspieszenia operacji \texttt{lookup}. Przetrzymywanie danych może być kompletne (cały wymiar przechowywany w pamięci podręcznej) lub częściowe (jedynie ostatnio używane wiersze). \texttt{CachedDimension} może przechowywać również nowe wiersze w chwili kiedy są one dodawane do hurtowni. W momencie inicjalizacji instancji istnieje możliwość edycji dodatkowych parametrów tak samo jak miało to miejsce w przypadku klasy \texttt{Dimension}. Dodatkowo, istnieje możliwość ustawienia wielkości dostępnej pamięci podręcznej oraz zadecydowania, czy pamięć podręczna powinna być odrazu wypełniona danymi z hurtowni, czy dopiero w momencie użycia wierszy. Ostatnimi opcjami jest możliwość ustawienia flagi oznaczającej, czy nowo wstawiane wiersze mają być przechowywane w pamięci podręcznej. Przykład:
	\lstinputlisting[language=Python]{./listings/example02.py}
	
	\item[\texttt{SlowlyChangingDimension}] wspiera pierwszy oraz drugi typ zmian w wolno zmieniających się wymiarach. w momencie utworzenia instancji, może ona być identycznie skonfigurowana jak obiekt klasy \texttt{Dimension}. Dodatkowo, nazwy atrybutów przechowujących informacje o zmianach typu drugiego w wymiarze hurtowni danych powinny zostać ustawione -- pozostałe parametry są opcjonalne. Istnieje możliwość ustawienia parametrów ,,\textit{from date}'' i ,,\textit{to date}'' informujących, kiedy z danego wiersza można korzystać -- w tym celu można również przypisać funkcję obliczającą obie te daty -- w przypadku jej braku pobierana jest data aktualna). Podobnie jak poprzednik, \texttt{SlowlyChangingDimension} udostępnia te same metody co klasa bazowa. W tym przypadku występuje przeciążenie funkcji \texttt{lookup}, która zwraca teraz wartość klucza najnowszej wersji. W celu obsługi wersjonowania, udostępniono metodę \texttt{scdensure}. Metoda ta jako parametr przyjmuje wiersz -- w pierwszej kolejności wykonuje \texttt{lookup}, a następnie sprawdza czy wprowadzone zostały jakieś zmiany. Jeżeli pole posiada przypisane zmiany typu pierwszego i modyfikacja zostanie wykryta, wykona się operacja aktualizacji pola. W przypadku zmian dla pola o zmianach typu drugiego, zostanie utworzona kopia całego wiersza i dodana do wymiaru. Podobnie jak \texttt{ensure}, w przypadku nieobecności wiersza podanego jako parametr wykonuje operację wstawienia go do bazy. Przykład:
	\lstinputlisting[language=Python]{./listings/example03.py}
	
	\item[\texttt{SnowflakedDimension}] wspiera wypełnianie wymiarów w schemacie płatka śniegu -- jest to schemat wielotabelowy, w którym jedna tabela odpowiada każdemu poziomowi w hierarchii wymiaru. Ponieważ tabele są powiązane za pomocą referencji, tablica faktów przechowuje referencje do jednej z nich -- odniesienie się do dowolnej innej jest możliwe za pomocą łańcucha referencji. W przypadku \texttt{SnowflakedDimension} wymiar przestaje reprezentować pojedynczą tabelę -- każda tablica w schemacie płatka śniegu reprezentuje część wymiaru. Kompletny jego obraz można uzyskać przez operację złączenia tabel. Najważniejszą zaletą \texttt{SnowflakedDimension} jest niejawne wielokrotne wywoływanie funkcji \texttt{ensure} -- w przypadku dodawania nowych wierszy metoda automatycznie wywołuje funkcję dodającą na każdym poziomie hierarchii powiązanym ze wstawianym rekordem. \\
	Instancja tej klasy jest tworzona z innych obiektów typu \texttt{Dimension} (w tym z klas pochodnych). Programista jest odpowiedzialny za utworzenie instancji każdej tabeli należącej do wymiaru w schemacie płatka śniegu i podanie ich jako parametry do utworzenia obiektu klasy \texttt{SnowflakedDimension}. Przykład deklaracji obiektu:
	
	\lstinputlisting[language=Python]{./listings/example04.py}
	
	Podany argument jest listą par, w których pierwszy element przechowuje referencję do każdego wymiaru drugiego elementu (drugi element sam w sobie również może być listą). Przeciążona metoda \texttt{lookup} wywołuje swój odpowiednik dla wymiaru obiektu będącego korzeniem drzewa tabel (implementacja zakłada, że atrybuty podglądu należą do tablicy leżącej najbliżej tablicy faktów). W przeciwnym wypadku, programista może użyć \texttt{lookup} lub \texttt{ensure} na wymiarze umieszczonym w dalszej odległości od korzenia i wykorzystać zwrócone wartości kluczy jako atrybuty podglądu dla \texttt{SnowflakedDimension}. Metoda \texttt{getbykey} może przyjąć dodatkowy argument decydujący, czy w pełni wypełniony wiersz ma zostać zwrócony (np. w przypadku, gdy konieczne było wykonanie operacji złączenia tabel), czy tylko fragment pochodzący z korzenia. To samo dotyczy metody \texttt{getbyvals}. Funkcje \texttt{ensure} oraz \texttt{insert} oddziałują na cały wymiar w schemacie płatka poczynając od korzenia i przemieszczając się w stronę zewnętrznych tabel hierarchii. Podobnie jak poprzednik, klasa udostępnia metodę \texttt{scdensure} -- może być ona użyta jedynie w przypadku, gdy korzeń jest wymiarem typu \texttt{SlowlyChangingDimension}
	
	\item[\texttt{BulkDimension}] jest wymiarem wyspecjalizowanym do zwiększenia wydajności operacji wstawiania poprzez dodawanie wierszy w wielkich porcjach danych (tzw. partiach, ang. \textit{bulk}) wczytywanych z pliku oraz poprzez szybką operacją \texttt{lookup} korzystającą z pamięci podręcznej. Aby umożliwić korzystanie z tej klasy, baza danych nie może dokonywać transformacji danych w celu uniknięcia niespójności pomiędzy zawartością pamięci podręcznej oraz samej bazy. Dodatkową cechą \texttt{BulkDimension} jest wywoływanie przez metody \texttt{update} oraz \texttt{getbyvals} dodatkowej funkcji \texttt{endload}, która dokonuje wstawienia wszystkich wierszy przechowywanych w lokalnym pliku do bazy danych. Użytkownik musi zadeklarować funkcję ładowania partii. Metoda \texttt{getbykey} wymusza na wymiarze domyślne ładowanie partii, lecz może użyć pamięci podręcznej poprzez ustawienie flagi \texttt{cachefullrows}. Wywołanie \texttt{lookup} oraz \texttt{ensure} spowoduje wykorzystanie wyłącznie pamięci podręcznej i nie wykona żadnych operacji na bazie danych (wszystkie wiersze klasy przechowywane są w pamięci).
	
	Przykład funkcji ładującej partie oraz inicjalizacji obiektu klasy \texttt{BulkDimension}:
	\lstinputlisting[language=Python]{./listings/example05.py}
	
	\item[\texttt{CachedBulkDimension}] jest zoptymalizowaną wersją \texttt{BulkDimension} zorientowaną na ładowanie całych wymiarów za pomocą partii. Jest ona przystosowana do użycia z pamięcią podręczną o skończonym rozmiarze, zamiast o (potencjalnie) nieskończonej. To pozwala na użycie tej klasy na zbiorze danych zbyt dużym, aby zmieścić go wyłącznie w pamięci podręcznej.
	
\end{description}

\subsection{Tablice faktów}

Pygrametl oferuje wsparcie dla trzech klas reprezentujących tablice faktów (są to obiekty reprezentujące aktualne tabele bazy danych). Przyjmuje się, że tablica faktów posiada określoną liczbą atrybutów kluczowych oraz każdy z nich przechowuje referencję do tabeli w wymiarze.

\subsubsection{FactTable}

\texttt{FactTable} jest podstawową reprezentacją tablicy faktów. W momencie utworzenia instancji programista podaje informacje o nazwie tabeli faktów, nazwach kluczowych atrybutów oraz opcjonalnie nazwy atrybutów pomiarowych. Operacje na tablicy faktów wykonywane są za pomocą trzech metod, które jako główny parametr przyjmują dwa słowniki. Pierwszy słownik używany jest jako wiersz gdzie klucz oznacza nazwy kolumn, natomiast drugi jest zestawem mapowań używanych do przemianowywania kluczy w wierszu w przypadku gdy nie są zgodne z tablicą znajdującą się w bazie danych.

Funkcje klasy \texttt{FactTable}:
\begin{itemize}
	\item \texttt{\textbf{insert}} wstawia nowe fakty (podane w parametrze) prosto do tablicy faktów.
	\item \texttt{\textbf{lookup}} sprawdza czy hurtownia zawiera fakt z podaną kombinacją kluczy przechowujących referencje do wymiarów.
	\item \texttt{\textbf{ensure}} stanowi połączeniu obu powyższych metod -- dokonuje sprawdzenia czy wiersz istnieje przed  wstawieniem go.
\end{itemize}

Przykład wykorzystania tablicy faktów:

\lstinputlisting[language=Python]{./listings/example06.py}

Jeżeli zbiór faktów zawiera już istniejący w bazie danych, metoda \texttt{ensure} może zostać użyta zamiast \texttt{lookup} i \texttt{insert} osobno, zmieniając przy tym nazwę kolumny z '\texttt{itemid}' na '\texttt{productid}'. W powyższym przypadku drugi obiekt tablicy \texttt{newFacts} posiada identyczne wartości klucza jak jeden z wierszy już znajdujących się w \texttt{factTable} -- zostanie wykonane sprawdzenie, czy wartości są identyczne (jeżeli nie są, nastąpi błąd).

\subsubsection{BatchFactTable}

\texttt{BatchFactTable} dziedziczy po klasie \texttt{FactTable} i udostępnia te same metody. Zasadniczą różnicą jest wstawianie wierszy nie w momencie wywołania metody \texttt{insert} lecz czekanie aż ustawiona przez użytkownika liczba wierszy (partia, ang. \textit{batch}) będzie gotowa do wstawienia. Wielkość partii tabeli faktów ustalana jest w momencie inicjalizacji i zarówno metoda \texttt{insert} jak i \texttt{ensure} mogą przyczynić się do wypełniania partii. Celem nadrzędnym takiej implementacji jest zwiększenia wydajności aplikacji ETL poprzez rzadsze odwołania do hurtowni.

\subsubsection{BulkFactTable}

Tablica faktów \texttt{BulkFactTable} również wykonuje operację wstawiania za pomocą partii, jednak zapisuje fakty do tymczasowego pliku zamiast przetrzymywania ich w pamięci. Pozwala to na użycie zapisu porcji danych przy ograniczonych zasobach pamięciowych. Wadą takiego rozwiązania jest zablokowanie możliwości podglądu bazy danych bez częstego jednoczesnego czytania pliku tymczasowego, dlatego metody \texttt{lookup} i \texttt{ensure} są niedostępne. Podobnie jak w przypadku innych tabel faktów, metoda \texttt{ConnectionWrapper.commit} musi zostać wywołana w celu upewnienia się, że pozostałe zbiory faktów zostały wstawione do tablicy faktów, a transakcja została zatwierdzona. \\

W celu zapewnienia większej kontroli nad partiami danych, użytkownik może w trakcie inicjalizacji dodać dodatkowe parametry pozwalające m.in. wybrać znak rozdzielający (ang. \textit{delimiter}) oraz rozmiar partii. Wszystkie nowe atrybuty posiadają wartość domyślną za wyjątkiem \texttt{bulkloader}. \\

Dokładny sposób ładowania partii danych jest zależny od systemu zarządzania bazą danych, dlatego wymaga się zdefiniowania funkcji \texttt{\textbf{bulkloader}}. Jest to metoda wywoływana w momencie kiedy partia jest wypełniona i ma zostać wykonana operacja załadowania danych do hurtowni. Parametry, jakie musi przyjąć funkcja aby jej sygnatura zgadzała się z sygnaturą funkcji \texttt{bulkloader}:

\begin{itemize}
	\item name -- nazwa tabeli w hurtowni danych.
	\item attributes -- lista zawierająca sekwencję atrybutów składających się na klucz główny tabeli faktów.
	\item fieldsep -- łańcuch znaków używany do separacji pól w pliku tymczasowym
	\item rowsep -- string używany do separacji wierszy w pliku tymczasowym
	\item nullval -- w przypadku podania tej wartości zajmuje ona miejsce \texttt{None} jako wartość pusta.
	\item filehandle -- nazwa pliku lub obiekt pliku. Podanie nazwy jest konieczne w przypadku, gdy ładowanie do bazy wywoływane jest przez SQL. 
\end{itemize}

Przykład użycia funkcji \texttt{bulkloader} oraz klasy \texttt{BulkFactTable}

\lstinputlisting[language=Python]{./listings/example07.py}