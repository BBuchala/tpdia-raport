\contentsline {part}{I\hspace {1em}HPCC Systems}{3}{part.1}
\contentsline {section}{\numberline {1}Struktura systemu}{3}{section.1.1}
\contentsline {section}{\numberline {2}Thor}{4}{section.1.2}
\contentsline {section}{\numberline {3}ROXIE}{4}{section.1.3}
\contentsline {section}{\numberline {4}ECL}{4}{section.1.4}
\contentsline {section}{\numberline {5}Oprogramowanie po\IeC {\'s}rednicz\IeC {\k a}ce}{5}{section.1.5}
\contentsline {subsection}{\numberline {5.1}Dali}{5}{subsection.1.5.1}
\contentsline {subsection}{\numberline {5.2}Sasha}{5}{subsection.1.5.2}
\contentsline {subsection}{\numberline {5.3}DFU}{7}{subsection.1.5.3}
\contentsline {subsection}{\numberline {5.4}Serwer ECL}{7}{subsection.1.5.4}
\contentsline {subsection}{\numberline {5.5}Serwer ESP}{7}{subsection.1.5.5}
\contentsline {section}{\numberline {6}Instalacja i uruchamianie systemu}{7}{section.1.6}
\contentsline {subsection}{\numberline {6.1}Instalacja r\IeC {\k e}czna}{8}{subsection.1.6.1}
\contentsline {subsection}{\numberline {6.2}Maszyna wirtualna}{8}{subsection.1.6.2}
\contentsline {subsection}{\numberline {6.3}ECL Watch}{9}{subsection.1.6.3}
\contentsline {subsection}{\numberline {6.4}ECL IDE}{11}{subsection.1.6.4}
\contentsline {part}{II\hspace {1em}Oracle Warehouse Builder i Oracle Data Integrator}{13}{part.2}
\contentsline {section}{\numberline {1}Oracle Warehouse Builder}{13}{section.2.1}
\contentsline {subsection}{\numberline {1.1}Wprowadzenie}{13}{subsection.2.1.1}
\contentsline {subsubsection}{\numberline {1.1.1}Og\IeC {\'o}lne informacje}{13}{subsubsection.2.1.1.1}
\contentsline {subsubsection}{\numberline {1.1.2}Om\IeC {\'o}wienie poj\IeC {\k e}\IeC {\'c}}{13}{subsubsection.2.1.1.2}
\contentsline {subsection}{\numberline {1.2}Instalacja}{14}{subsection.2.1.2}
\contentsline {subsubsection}{\numberline {1.2.1}Instalacja na skonfigurowanej bazie danych Oracle Database 11.2}{14}{subsubsection.2.1.2.1}
\contentsline {subsubsection}{\numberline {1.2.2}Instalacja r\IeC {\k e}czna}{14}{subsubsection.2.1.2.2}
\contentsline {subsection}{\numberline {1.3}\IeC {\'S}rodowisko}{14}{subsection.2.1.3}
\contentsline {section}{\numberline {2}Oracle Data Integrator}{14}{section.2.2}
\contentsline {subsection}{\numberline {2.1}Wprowadzenie}{14}{subsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Og\IeC {\'o}lne informacje}{14}{subsubsection.2.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}Architektura systemu}{15}{subsubsection.2.2.1.2}
\contentsline {subsubsection}{\numberline {2.1.3}Repozytorium}{15}{subsubsection.2.2.1.3}
\contentsline {subsection}{\numberline {2.2}Instalacja}{16}{subsection.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Maszyna wirtualna}{16}{subsubsection.2.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}Instalacja r\IeC {\k e}czna}{16}{subsubsection.2.2.2.2}
\contentsline {paragraph}{Rdze\IeC {\'n} programu}{16}{subsubsection.2.2.2.2}
\contentsline {paragraph}{Baza danych}{17}{subsubsection.2.2.2.2}
\contentsline {paragraph}{Utworzenie repozytorium}{18}{lstnumber.-12.1}
\contentsline {subsection}{\numberline {2.3}\IeC {\'S}rodowisko}{18}{subsection.2.2.3}
\contentsline {subsubsection}{\numberline {2.3.1}Oracle Data Integrator (ODI Studio)}{18}{subsubsection.2.2.3.1}
\contentsline {paragraph}{O \IeC {\'s}rodowisku}{18}{subsubsection.2.2.3.1}
\contentsline {paragraph}{Skrypt uruchamiaj\IeC {\k a}cy}{19}{subsubsection.2.2.3.1}
\contentsline {paragraph}{Pod\IeC {\l }\IeC {\k a}czenie do repozytorium Master}{20}{lstnumber.-15.1}
\contentsline {paragraph}{Tworzenie nowego agenta}{21}{figure.14}
\contentsline {paragraph}{Utworzenie nowej domeny dla agenta}{21}{figure.14}
\contentsline {paragraph}{Interfejs u\IeC {\.z}ytkownika}{22}{figure.15}
\contentsline {subsubsection}{\numberline {2.3.2}Baza danych (SQL Developer)}{22}{subsubsection.2.2.3.2}
\contentsline {paragraph}{O \IeC {\'s}rodowisku}{22}{subsubsection.2.2.3.2}
\contentsline {paragraph}{Uruchomienie}{22}{subsubsection.2.2.3.2}
\contentsline {paragraph}{Interfejs u\IeC {\.z}ytkownika}{22}{lstnumber.-17.1}
\contentsline {part}{III\hspace {1em}Pygrametl}{23}{part.3}
\contentsline {section}{\numberline {1}Wst\IeC {\k e}p}{23}{section.3.1}
\contentsline {section}{\numberline {2}Instalacja i uruchamianie}{24}{section.3.2}
\contentsline {subsection}{\numberline {2.1}Instalacja za pomoc\IeC {\k a} Pip}{24}{subsection.3.2.1}
\contentsline {subsection}{\numberline {2.2}Instalacja za pomoc\IeC {\k a} Condy}{24}{subsection.3.2.2}
\contentsline {subsection}{\numberline {2.3}Instalacja z serwisu Github}{25}{subsection.3.2.3}
\contentsline {section}{\numberline {3}Om\IeC {\'o}wienie funkcjonalno\IeC {\'s}ci}{25}{section.3.3}
\contentsline {subsection}{\numberline {3.1}Zarys}{25}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.2}Wspierane \IeC {\'z}r\IeC {\'o}d\IeC {\l }a danych}{25}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3}Wymiary}{26}{subsection.3.3.3}
\contentsline {subsubsection}{\numberline {3.3.1}Hierarchia}{26}{subsubsection.3.3.3.1}
\contentsline {subsubsection}{\numberline {3.3.2}Klasa Dimension}{26}{subsubsection.3.3.3.2}
\contentsline {subsubsection}{\numberline {3.3.3}Klasy pochodne}{27}{subsubsection.3.3.3.3}
\contentsline {subsection}{\numberline {3.4}Tablice fakt\IeC {\'o}w}{29}{subsection.3.3.4}
\contentsline {subsubsection}{\numberline {3.4.1}FactTable}{29}{subsubsection.3.3.4.1}
\contentsline {subsubsection}{\numberline {3.4.2}BatchFactTable}{30}{subsubsection.3.3.4.2}
\contentsline {subsubsection}{\numberline {3.4.3}BulkFactTable}{30}{subsubsection.3.3.4.3}
\contentsline {part}{IV\hspace {1em}Por\IeC {\'o}wnanie system\IeC {\'o}w}{32}{part.4}
\contentsline {section}{\numberline {1}Informacje podstawowe}{32}{section.4.1}
\contentsline {subsection}{\numberline {1.1}Producent oprogramowania}{32}{subsection.4.1.1}
\contentsline {subsection}{\numberline {1.2}Data pierwszego wydania}{32}{subsection.4.1.2}
\contentsline {subsection}{\numberline {1.3}Typ licencji}{32}{subsection.4.1.3}
\contentsline {subsection}{\numberline {1.4}Platforma systemowa}{32}{subsection.4.1.4}
\contentsline {subsection}{\numberline {1.5}Wymagania sprz\IeC {\k e}towe}{32}{subsection.4.1.5}
\contentsline {subsection}{\numberline {1.6}Koszt}{32}{subsection.4.1.6}
\contentsline {section}{\numberline {2}Zakres mo\IeC {\.z}liwo\IeC {\'s}ci}{32}{section.4.2}
\contentsline {subsection}{\numberline {2.1}Rodzaj architektury}{32}{subsection.4.2.1}
\contentsline {subsection}{\numberline {2.2}J\IeC {\k e}zyk skryptowania}{33}{subsection.4.2.2}
\contentsline {subsection}{\numberline {2.3}Spos\IeC {\'o}b wczytywania danych}{33}{subsection.4.2.3}
\contentsline {subsection}{\numberline {2.4}Praca z SZBD}{33}{subsection.4.2.4}
\contentsline {subsection}{\numberline {2.5}Praca z plikami zewn\IeC {\k e}trznymi}{33}{subsection.4.2.5}
\contentsline {subsection}{\numberline {2.6}Wspieranie przetwarzania r\IeC {\'o}wnoleg\IeC {\l }ego}{33}{subsection.4.2.6}
\contentsline {subsection}{\numberline {2.7}Skalowalno\IeC {\'s}\IeC {\'c}}{33}{subsection.4.2.7}
\contentsline {subsection}{\numberline {2.8}Mo\IeC {\.z}liwo\IeC {\'s}\IeC {\'c} monitorowania}{33}{subsection.4.2.8}
\contentsline {section}{\numberline {3}Wydajno\IeC {\'s}\IeC {\'c}}{33}{section.4.3}
\contentsline {section}{Za\IeC {\l }\IeC {\k a}cznik}{35}{section*.1}
\contentsline {section}{\numberline {A}Zawarto\IeC {\'s}\IeC {\'c} pami\IeC {\k e}ci Flash}{35}{Dodatek.1.A}
