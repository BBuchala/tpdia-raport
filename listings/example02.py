testdim = CachedDimension(name='test',
			key='testid',
			defaultidvalue=-1,
			attributes=['testname', 'testauthor'],
			lookupatts=['testname'],
			cachesize=500,
			prefill=True,
			cachefullrows=True)