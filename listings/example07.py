facts = [ {'storeid' : 1, 'productid' : 13, 'dateid' : 4, 'price': 50},
          {'storeid' : 2, 'productid' :  7, 'dateid' : 4, 'price': 75},
          {'storeid' : 1, 'productid' :  7, 'dateid' : 4, 'price': 50},
          {'storeid' : 3, 'productid' :  9, 'dateid' : 4, 'price': 25} ]

def pgbulkloader(name, attributes, fieldsep, rowsep, nullval, filehandle):
    cursor = conn.cursor()
    cursor.copy_from(file=filehandle, table=name, sep=fieldsep,
                     columns=attributes)

factTable = BulkFactTable(
    name='facttable',
    measures=['price'],
    keyrefs=['storeid', 'productid', 'dateid'],
    bulkloader=pgbulkloader)

for row in facts:
    factTable.insert(row)
conn.commit()
conn.close()
