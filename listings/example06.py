facts = [ {'storeid' : 1, 'productid' : 13, 'dateid' : 4, 'price': 50},
          {'storeid' : 2, 'productid' :  7, 'dateid' : 4, 'price': 75},
          {'storeid' : 1, 'productid' :  7, 'dateid' : 4, 'price': 50},
          {'storeid' : 3, 'productid' :  9, 'dateid' : 4, 'price': 25} ]

for row in facts:
    factTable.insert(row)
conn.commit()

factTable.lookup({'storeid' : 1, 'productid' : 13, 'dateid' : 4})


newFacts = [ {'storeid' : 2, 'itemid' :  7, 'dateid' : 4, 'price': 75},
             {'storeid' : 1, 'itemid' :  7, 'dateid' : 4, 'price': 50},
             {'storeid' : 1, 'itemid' :  2, 'dateid' : 7, 'price': 150},
             {'storeid' : 3, 'itemid' :  3, 'dateid' : 6, 'price': 100} ]

for row in newFacts:
    factTable.ensure(row, True, {'productid' : 'itemid'})
conn.commit()
conn.close()