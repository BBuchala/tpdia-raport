pagedim = SlowlyChangingDimension(name='page',
			key='pageid',
			attributes=['url', 'size', 'validfrom', 'validto',
			'version', 'domainid' 'serverversionid'],
			lookupatts=['url'],
			fromatt='validfrom',
			fromfinder=pygrametl.datereader('lastmoddate'),
			toatt='validto',
			versionatt='version')