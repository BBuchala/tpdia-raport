def pgbulkloader(name, attributes, fieldsep, rowsep, nullval, filehandle):
		    cursor = conn.cursor()
		    cursor.copy_from(file=filehandle, table=name, sep=fieldsep,
		                     columns=attributes)

ProductDimension = BulkDimension(
		    name='product',
		    key='productid',
		    attributes=['name', 'category', 'price'],
		    lookupatts=['name'],
		    bulkloader=pgbulkloader)